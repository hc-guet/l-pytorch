import os
import sys

import torch
import PySimpleGUI

# Exercise-1-Conv2d-padding
def exercise_1():
    in_channels, out_channels = 5, 10
    width, height = 100, 100

    kernel_size = 3
    batch_size = 2

    input = torch.randn(batch_size, in_channels, width, height)

    conv_layer = torch.nn.Conv2d(in_channels=in_channels,
                                 out_channels=out_channels,
                                 kernel_size=kernel_size,
                                 stride=2,
                                 padding=1,
                                 bias=False)

    output = conv_layer(input)

    print('input.shape:', input.shape)
    print('output.shape:', output.shape)
    print('conv_layer.weight.shape:', conv_layer.weight.shape)

    # # (batch_size, in_channels, in_width, in_height)
    # input.shape: torch.Size([2, 5, 100, 100])
    #
    # # (batch_size, out_channels, out_width, out_height)
    # output.shape: torch.Size([2, 10, 50, 50])
    #
    # # (out_channels, in_channels, kernel_width, kernel_height)
    # conv_layer.weight.shape: torch.Size([10, 5, 3, 3])


# Exercise-2-stride
def exercise_2():
    input = [3, 4, 6, 5, 7,
             2, 4, 6, 8, 2,
             1, 6, 7, 8, 4,
             9, 7, 4, 6, 2,
             3, 7, 5, 4, 1]

    # (batch_size, in_channels, in_width, in_height)
    input = torch.Tensor(input).view(1, 1, 5, 5)

    conv_layer = torch.nn.Conv2d(in_channels=1,
                                 out_channels=1,
                                 kernel_size=3,
                                 stride=2,
                                 bias=False)

    # view(out_channels, in_channels, kernel_width, kernel_height)
    kernel = torch.Tensor([1, 2, 3, 4, 5, 6, 7, 8, 9]).view(1, 1, 3, 3)

    conv_layer.weight.data = kernel.data

    output = conv_layer(input)

    print(output)
    # tensor([[[[211., 262.],
    #           [251., 169.]]]], grad_fn=<MkldnnConvolutionBackward>)


# Exercise-3-maxpooling
def exercise_3():
    input = [3, 4, 6, 5,
             2, 4, 6, 8,
             1, 6, 7, 8,
             9, 7, 4, 6,
             1, 6, 7, 8,
             9, 7, 4, 6,
             3, 4, 6, 5,
             2, 4, 6, 8,
             ]

    # (batch_size, in_channels, in_width, in_height)
    input = torch.Tensor(input).view(1, 2, 4, 4)

    maxpooling_layer = torch.nn.MaxPool2d(kernel_size=3)
    output = maxpooling_layer(input)  # 为每个通道单独做池化
    print(output)
    # tensor([[[[4., 8.],
    #           [9., 8.]],
    #
    #          [[9., 8.],
    #           [4., 8.]]]])

    # (batch_size, channels, (pooled_size))
    print(output.shape)
    # torch.Size([1, 2, 2, 2])


# Exercise-4-Conv1d
def exercise_4():
    # (batch_size, in_channels, width, height)
    input = torch.randn(2, 2, 5, 4)

    # (in_channels, out_channels, kernel_size)
    # 注意这里 kernel_size.width 和 in_width 相等
    conv1d_layer = torch.nn.Conv1d(2, 2, kernel_size=(5, 3))

    # Conv1d的意思是只沿着竖直方向卷积，2d是先水平卷积，再竖直卷积
    # 一般来说，一维卷积nn.Conv1d用于文本数据，只对宽度进行卷积，对高度不卷积
    output = conv1d_layer(input)

    print(output)
    # (batch_size, out_channels, out_width, out_height)
    print(output.shape)



exercise_4()
PySimpleGUI.popup(os.path.basename(sys.argv[0]))    # 程序运行完弹窗提示


# 总结：
# 1.卷积的步长在水平和垂直方向都作用
# 2.池化的步长就和size相同
# 3.卷积或池化由于步长导致剩下的图像不够核大小的时候，直接丢弃，放弃卷积或池化操作
# 换句话说，只有图像的部分全在核内时才进行操作
