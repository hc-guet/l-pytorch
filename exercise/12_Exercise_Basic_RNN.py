import os
import sys

import PySimpleGUI
import torch


def rnncell():
    # Parameters
    batch_size = 1
    seq_len = 3
    input_size = 4
    hidden_size = 2

    cell = torch.nn.RNNCell(input_size=input_size, hidden_size=hidden_size)

    # (seqLen, batchSize, features/inputSize)
    dataset = torch.randn(seq_len, batch_size, input_size)
    hidden = torch.zeros(batch_size, hidden_size)  # 要先设定一个初始的hidden_0

    for idx, input in enumerate(dataset):  # 遍历seqLen
        print('=' * 20, idx, '=' * 20)
        print('Input size:', input.shape)  # (batchSiZe, inputSize)

        hidden = cell(input, hidden)

        # RNNCell中，hidden=out
        print('outputs size:', hidden.shape)  # (batchSize, hiddenSize)
        print(hidden)


def rnn():
    batch_size = 1
    seq_len = 3
    input_size = 4
    hidden_size = 2
    num_layers = 1
    cell = torch.nn.RNN(input_size=input_size,
                        hidden_size=hidden_size,
                        num_layers=num_layers)

    # (seqLen, batchSize, inputSize)
    inputs = torch.randn(seq_len, batch_size, input_size)
    hidden = torch.zeros(num_layers, batch_size, hidden_size)
    out, hidden = cell(inputs, hidden)

    print('Inputs size:', inputs.shape)  # RNN连同seqLen一次性输入
    print('Output size:', out.shape)  # (seqLen, batchSize, hiddenSize)
    print('Output:', out)
    print('Hidden size:', hidden.shape)  # (numLayers, batchSize, hiddenSize)
    print('Hidden:', hidden)


# rnncell()
rnn()
PySimpleGUI.popup(os.path.basename(sys.argv[0]))  # 程序运行完弹窗提示
