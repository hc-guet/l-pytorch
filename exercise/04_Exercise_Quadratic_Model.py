# 二次方模型
import torch

x_data = [1.0, 2.0, 3.0]
y_data = [4.0, 11.0, 22.0]

# Tensor自动求导，与之运算的变量也会变成Tensor
w1 = torch.Tensor([1.0])
w2 = torch.Tensor([1.0])
b = torch.Tensor([1.0])

w1.requires_grad = True
w2.requires_grad = True
b.requires_grad = True

lr = 0.01  # 学习率
EPOCH = 1000


def forward(x):
    return w1 * x * x + w2 * x + b


def loss(x, y):
    pre_y = forward(x)
    return (pre_y - y) ** 2


print("\nprediction before training:", 4, '{:.2f}\n'.format(forward(4).item()))
for epoch in range(EPOCH):
    for x, y in zip(x_data, y_data):
        l = loss(x, y)  # 每次计算单个样本的loss
        l.backward()  # 反向传播

        w1.data -= lr * w1.grad.data  # 更新w1
        w2.data -= lr * w2.grad.data  # 更新w2
        b.data -= lr * b.grad.data  # 更新b

        w1.grad.data.zero_()  # 梯度清零
        w2.grad.data.zero_()
        b.grad.data.zero_()

    # 下面的l是一轮训练最后的(x,y)产生的loss
    print("epoch:", epoch, 'loss:{:.2f}\n'.format(l.item()))

print("\nprediction after training:", 4, '{:.2f}'.format(forward(4).item()))
print("训练出来的系数：", '{:.2f}'.format(w1.item()), '{:.2f}'.format(w2.item()), '{:.2f}'.format(b.item()))
