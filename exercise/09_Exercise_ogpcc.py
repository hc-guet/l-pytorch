import torch
import numpy as np
import pandas as pd
from torch.utils.data import Dataset, DataLoader
from sklearn.metrics import accuracy_score
import matplotlib.pyplot as plt

device = 'cuda' if torch.cuda.is_available() else 'cpu'  # 'cuda'/'cpu'

BATCH_SIZE = 512
EPOCH = 1


# 构建OGPCC数据集类
class OGPCC(Dataset):
    def __init__(self, train):
        # 构建数据样本
        self.train = train
        self.data = pd.read_csv('../dataset/otto-group-product-classification-challenge/train.csv')
        if self.train:
            # 随机选取80%作为训练集，不可按索引顺序取，数据会不全面
            # self.x_data = self.x_data[:int(self.data.shape[0] * 0.8)]
            # self.y_data = self.y_data[:int(self.data.shape[0] * 0.8)]
            self.data = self.data.sample(frac=0.8, replace=False, random_state=1, axis=0)
            ### 正式训练要训练所有数据 ###
            # self.x_data = self.x_data
            self.len = self.data.shape[0]
        else:
            # 20%作为验证集
            self.data = self.data.sample(frac=0.2, replace=False, random_state=1, axis=0)
            self.len = self.data.shape[0]

        # 处理标签
        self.data = process_data(self.data)
        # 提取特征
        self.x_data = self.data[['feat_' + str(i) for i in range(1, 94)]]
        self.y_data = self.data['target']
        # 数据转化为Tensor
        self.x_data, self.y_data = torch.Tensor(self.x_data.values), torch.LongTensor(self.y_data.values)

    def __getitem__(self, index):
        # 根据数据索引获取样本
        return self.x_data[index], self.y_data[index]

    def __len__(self):
        # 返回数据长度
        return self.len


# 这里对数据进行简单的处理，数据清洗，填充等
def process_data(data):
    # 将class映射到index
    data['target'] = data['target']. \
        map({'Class_1': 0, 'Class_2': 1,
             'Class_3': 2, 'Class_4': 3,
             'Class_5': 4, 'Class_6': 5,
             'Class_7': 6, 'Class_8': 7,
             'Class_9': 8
             })
    return data


# 训练集验证集数据对象
train_set = OGPCC(train=True)
validation_set = OGPCC(train=False)

# 训练集验证集数据加载对象
train_loader = DataLoader(
    dataset=train_set,
    batch_size=BATCH_SIZE,
    shuffle=True,
    # num_workers=2
)

validation_loader = DataLoader(
    dataset=validation_set,
    batch_size=BATCH_SIZE,
    shuffle=True,  # 测试集不打乱有利于观察结果
    # num_workers=2
)


# 测试集
def get_test_set():
    test_set = pd.read_csv('../dataset/otto-group-product-classification-challenge/test.csv')
    # 测试集要处理：Age Fare Cabin Sex
    id = test_set['id']  # 先获取编号，最后保存预测结果
    # 提取特征
    test_set = test_set[['feat_' + str(i) for i in range(1, 94)]]
    test_set = torch.Tensor(test_set.values).to(device)
    return id, test_set


class Model(torch.nn.Module):
    def __init__(self):
        super(Model, self).__init__()
        self.linear1 = torch.nn.Linear(93, 128)
        self.linear2 = torch.nn.Linear(128, 256)
        self.linear3 = torch.nn.Linear(256, 128)
        self.linear4 = torch.nn.Linear(128, 64)
        self.linear5 = torch.nn.Linear(64, 32)
        self.linear6 = torch.nn.Linear(32, 16)
        self.linear7 = torch.nn.Linear(16, 9)
        self.relu = torch.nn.ReLU()
        # self.softmax = torch.nn.Softmax(dim=1)

    def forward(self, x):
        # x = self.softmax(x)
        x = self.relu(self.linear1(x))
        x = self.relu(self.linear2(x))
        x = self.relu(self.linear3(x))
        x = self.relu(self.linear4(x))
        x = self.relu(self.linear5(x))
        x = self.relu(self.linear6(x))
        x = self.linear7(x)
        return x


model = Model().to(device)
loss_func = torch.nn.CrossEntropyLoss()  # 交叉熵
optimizer = torch.optim.Adam(model.parameters(), lr=0.01)


# optimizer = torch.optim.SGD(model.parameters(), lr=0.01, momentum=0.5)  # SGD加上动量训练效果更好


def train(epoch):
    # 训练模型
    for batch_idx, (input, target) in enumerate(train_loader):
        input, target = input.to(device), target.to(device)
        prediction = model(input)
        loss = loss_func(prediction, target)

        loss_list.append(loss.item())
        plt.plot([i for i in range(len(loss_list))], loss_list)
        plt.show()
        plt.pause(0.1)

        print(epoch, batch_idx, loss.item())

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()


def eval(epoch):
    # 测试模型 评价指标 绘图
    # 测试模型 评价指标
    acc = 0
    total = 0
    with torch.no_grad():  # 强制之后的内容不进行计算图构建
        print('+' * 20)
        for batch_idx, (input, target) in enumerate(validation_loader):
            input, target = input.to(device), target.to(device)
            output = model(input)
            loss = loss_func(output, target)
            # acc = accuracy_score(prediction, target)  # sklearn的库函数accuracy_score()只能使用cpu计算

            _, prediction = torch.max(output.data, dim=1)  # torch.max()返回最大值和最大值对应的下标
            total += input.size(0)
            acc += (prediction == target).sum().item()
            print(epoch, batch_idx, loss.item(), acc / total)
        print('+' * 20)


# 处理输出保存为可提交的文件
def process_output(id, pre_y):
    # 将输出类别变为独热码
    pre_y = pre_y.cpu().data.numpy()
    pre_y = (pre_y == pre_y.max(axis=1)[:, None]).astype(int)
    # _, predicted = torch.max(pre_y, dim=1)
    # pre_y = pd.get_dummies(predicted.cpu().data)   # 另一种变为one-hot向量的方法，但不够健壮
    result = pd.DataFrame(pre_y, columns=['Class_' + str(i) for i in range(1, 10)])
    result.insert(loc=0, column='id', value=id)
    # print(result)
    result.to_csv('ogpcc_predict.csv', index=False)  # 保存结果


if __name__ == '__main__':
    plt.figure(figsize=(3, 2))
    plt.ion()
    loss_list = []
    # 训练模型  ##########################################
    for epoch in range(EPOCH):
        train(epoch)
        # if epoch % 10 == 9:  # 每训练100次就评估一下模型
        eval(epoch)
    # 保存整个模型
    torch.save(model, 'ogpcc.pkl')  # 读取用torch.load()
    #######################################################

    # 使用模型得到结果
    id, test_set = get_test_set()  # 获取测试集
    with torch.no_grad():
        pre_y = model(test_set)
    process_output(id, pre_y)
    # plt.pause(0)
    plt.savefig('loss.png')
