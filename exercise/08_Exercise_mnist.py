# 以下只是一套伪代码
import torch
from torchvision import transforms, datasets
from torch.utils.data import Dataset, DataLoader
from sklearn.metrics import accuracy_score


# 标准数据集类有时需要我们自己去实现
class MyData(Dataset):
    def __init__(self):
        # 构建数据样本
        pass

    def __getitem__(self, index):
        # 根据数据索引获取样本
        pass

    def __len__(self):
        # 返回数据个数
        pass


# 训练集和测试集数据对象
train_data = datasets.MNIST(root='../dataset/mnist',
                            train=True,
                            transform=transforms.ToTensor(),
                            download=True)

test_data = datasets.MNIST(root='../dataset/mnist',
                           train=False,
                           transform=transforms.ToTensor(),
                           download=True)

# 训练集测试集数据加载对象
train_loader = DataLoader(
    dataset=train_data,
    batch_size=32,
    shuffle=True,
    num_workers=2
)

test_loader = DataLoader(
    dataset=test_data,
    batch_size=32,
    shuffle=False,  # 测试集不打乱有利于观察结果
    num_workers=2
)


class MyModel(torch.nn.Module):
    def __init__(self):
        super(MyModel, self).__init__()

    def forward(self, x):
        pass


model = MyModel().cuda()
loss_func = torch.nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(model.parameters(), lr=0.01)


def train(epoch):
    # 训练模型
    for batch_idx, (input, target) in enumerate(train_loader):
        input, target = input.cuda(), target.cuda()
        prediction = model(input)
        loss = loss_func(prediction, target)
        print(epoch, batch_idx, loss.item())

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()


def eval(epoch):
    # 测试模型 评价指标 绘图
    with torch.no_grad():  # 强制之后的内容不进行计算图构建。
        for batch_idx, (input, target) in enumerate(test_loader):
            prediction = model(input)
            loss = loss_func(prediction, target)
            acc = accuracy_score(prediction, target)
            print(epoch, batch_idx, loss, acc)
            # 还可以保存实验数据方便绘图


if __name__ == '__main__':
    for epoch in range(1000):
        train(epoch)
        if epoch % 100 == 99:  # 每训练100次就评估一下模型
            eval(epoch)
