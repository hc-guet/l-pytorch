# 根据第二课线性模型所画的图
# 我们要找到mse最小的点对应的w值，也就是梯度为0的w值

x_data = [1.0, 2.0, 3.0]
y_data = [2.0, 4.0, 6.0]

w = 1.0  # 权重初始值
lr = 0.01  # 学习率
EPOCH = 100


def forward(x):
    return w * x


def loss(x, y):
    pre_y = forward(x)
    return (pre_y - y) ** 2


def gradient(x, y):
    grad = 2 * x * (forward(x) - y)  # 计算单个样本的梯度
    return grad


print("w before:", w)
print("\nprediction before training:", 4, forward(4))
for epoch in range(EPOCH):
    for x, y in zip(x_data, y_data):
        grad_val = gradient(x, y)  # 计算一个样本的梯度
        w -= lr * grad_val  # 根据一个样本的梯度更新w
        l = loss(x, y)  # 每次计算单个样本的loss

    # 下面的l是一轮训练最后的(x,y)产生的loss
    print("\nepoch:", epoch, 'w:{:.2f}'.format(w), 'loss:{:.2f}'.format(l))

print("\nprediction after training:", 4, '{:.2f}'.format(forward(4)))
