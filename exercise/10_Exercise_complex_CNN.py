import torch
from torchvision import transforms, datasets
from torch.utils.data import DataLoader
import matplotlib.pyplot as plt

batch_size = 64
device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

transform = transforms.Compose([
    transforms.ToTensor(),  # 转化为Tensor
    transforms.Normalize((0.1307,), (0.3081,))  # 归一化，两个参数分别为均值和标准差
])

# 训练集和测试集数据对象
train_data = datasets.MNIST(root='../dataset/mnist',
                            train=True,
                            transform=transform,
                            download=True)
test_data = datasets.MNIST(root='../dataset/mnist',
                           train=False,
                           transform=transform,
                           download=True)

# 训练集测试集数据加载对象
train_loader = DataLoader(
    dataset=train_data,
    shuffle=True,
    batch_size=batch_size
)

test_loader = DataLoader(
    dataset=test_data,
    shuffle=False,
    batch_size=batch_size
)


# 定义模型
class Net(torch.nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = torch.nn.Conv2d(1, 10, kernel_size=2, padding=1)
        self.conv2 = torch.nn.Conv2d(10, 20, kernel_size=2, padding=1)
        self.conv3 = torch.nn.Conv2d(20, 10, kernel_size=4)
        self.pooling = torch.nn.MaxPool2d(kernel_size=2)
        self.linear1 = torch.nn.Linear(40, 64)
        self.linear2 = torch.nn.Linear(64, 32)
        self.linear3 = torch.nn.Linear(32, 10)

    def forward(self, x):
        batch_size = x.size(0)
        x = torch.relu(self.pooling(self.conv1(x)))
        x = torch.relu(self.pooling(self.conv2(x)))
        x = torch.relu(self.pooling(self.conv3(x)))
        x = x.view(batch_size, -1)
        x = torch.relu(self.linear1(x))
        x = torch.relu(self.linear2(x))
        x = self.linear3(x)
        return x


model = Net().to(device)
loss_func = torch.nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(model.parameters(), lr=0.01)


def train(epoch):
    # 训练模型
    running_loss = 0.0
    for batch_idx, (input, target) in enumerate(train_loader):
        images, labels = input.to(device), target.to(device)
        prediction = model(images)
        loss = loss_func(prediction, labels)

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        running_loss += loss.item()
        if batch_idx % 300 == 299:
            print('[%d, %5d] loss: %.3f' % (epoch + 1, batch_idx + 1, running_loss / 300))
            running_loss = 0.0


def eval(epoch):
    # 测试模型 评价指标
    correct = 0
    total = 0
    with torch.no_grad():  # 强制之后的内容不进行计算图构建。
        for batch_idx, (input, target) in enumerate(test_loader):
            images, labels = input.to(device), target.to(device)
            output = model(images)
            _, prediction = torch.max(output.data, dim=1)  # torch.max()返回最大值和最大值对应的下标
            total += labels.size(0)
            correct += (prediction == labels).sum().item()
    print('epoch: %d, acc on test dataset: %d %%' % (epoch, 100 * correct / total))
    acc_list.append(correct / total)


if __name__ == '__main__':
    epoch_list = []
    acc_list = []
    for epoch in range(20):
        train(epoch)
        if epoch % 10 == 9:  # 每训练10次就评估一下模型
            epoch_list.append(epoch)
            eval(epoch)

    plt.figure()
    plt.plot(epoch_list, acc_list)
    plt.xlabel('Epoch')
    plt.ylabel('Accuracy')
    plt.grid()
    plt.show()
