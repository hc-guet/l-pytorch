import torch
import numpy as np
import pandas as pd
from torch.utils.data import Dataset, DataLoader
from sklearn.metrics import accuracy_score
import matplotlib.pyplot as plt

device = 'cuda' if torch.cuda.is_available() else 'cpu'  # 'cuda'/'cpu'

BATCH_SIZE = 64


# 标准数据集类有时需要我们自己去实现
class TitanicData(Dataset):
    def __init__(self, train):
        # 构建数据样本
        self.train = train
        self.data = pd.read_csv('../dataset/titanic/train.csv')
        self.data = process_data(self.data)
        # 舍弃一些难以处理的字符数据
        self.x_data = self.data[['Pclass', 'Sex', 'Age', 'SibSp', 'Parch', 'Fare', 'Embarked']]
        self.y_data = self.data['Survived']
        if self.train:
            # 取80%作为训练集
            # self.x_data = self.x_data[:int(self.data.shape[0] * 0.8)]
            # self.y_data = self.y_data[:int(self.data.shape[0] * 0.8)]
            ### 正式训练要训练所有数据 ###
            self.x_data = self.x_data
            self.y_data = self.y_data
            self.len = self.x_data.shape[0]
        else:
            # 20%作为验证集
            self.x_data = self.x_data[int(self.data.shape[0] * 0.8):]
            self.y_data = self.y_data[int(self.data.shape[0] * 0.8):]
            self.len = self.x_data.shape[0]

        # 这里的标签y_data要变成一个Nx1的矩阵，即每行x_data对应一个[0/1]
        self.x_data, self.y_data = torch.Tensor(self.x_data.values), torch.Tensor(self.y_data.values.reshape((-1, 1)))

    def __getitem__(self, index):
        # 根据数据索引获取样本
        return self.x_data[index], self.y_data[index]

    def __len__(self):
        # 返回数据长度
        return self.len


# 这里对数据进行简单的处理，数据清洗，填充等
def process_data(data):
    # 数据集要处理：Age Cabin Embarked Sex Fare
    data['Fare'].fillna(data['Fare'].mean(), inplace=True)  # 处理测试集中Fare
    data['Age'].fillna(data['Age'].median(), inplace=True)  # Age用中值填充
    data['Sex'] = data['Sex'].map({'male': 1, 'female': 0})  # Sex用0/1代替
    data['Embarked'].fillna('S', inplace=True)  # Embarked用众数填充
    data['Embarked'] = data['Embarked'].map({'C': 0, 'Q': 1, 'S': 2})  # Embarked用0/1/2代替
    return data


# 训练集验证集数据对象
train_set = TitanicData(train=True)
validation_set = TitanicData(train=False)

# 训练集验证集数据加载对象
train_loader = DataLoader(
    dataset=train_set,
    batch_size=BATCH_SIZE,
    shuffle=True,
    # num_workers=2
)

validation_loader = DataLoader(
    dataset=validation_set,
    batch_size=BATCH_SIZE,
    shuffle=True,  # 测试集不打乱有利于观察结果
    # num_workers=2
)


# 测试集
def get_test_set():
    test_set = pd.read_csv('../dataset/titanic/test.csv')
    # 测试集要处理：Age Fare Cabin Sex
    Id_Survived = test_set['PassengerId']  # 先获取乘客编号，最后保存预测结果
    test_set = test_set[['Pclass', 'Sex', 'Age', 'SibSp', 'Parch', 'Fare', 'Embarked']]
    test_set = process_data(test_set)
    test_set = torch.Tensor(test_set.values).to(device)
    return Id_Survived, test_set


class Model(torch.nn.Module):
    def __init__(self):
        super(Model, self).__init__()
        self.linear1 = torch.nn.Linear(7, 16)
        self.linear2 = torch.nn.Linear(16, 32)
        self.linear3 = torch.nn.Linear(32, 32)
        self.linear4 = torch.nn.Linear(32, 16)
        self.linear5 = torch.nn.Linear(16, 8)
        self.linear6 = torch.nn.Linear(8, 4)
        self.linear7 = torch.nn.Linear(4, 1)
        self.relu = torch.nn.ReLU()
        self.sigmiod = torch.nn.Sigmoid()  # 二分类用Sigmoid

    def forward(self, x):
        x = self.relu(self.linear1(x))
        x = self.relu(self.linear2(x))
        x = self.relu(self.linear3(x))
        x = self.relu(self.linear4(x))
        x = self.relu(self.linear5(x))
        x = self.relu(self.linear6(x))
        x = self.sigmiod(self.linear7(x))
        return x


model = Model().to(device)
loss_func = torch.nn.BCELoss()  # 二分类交叉熵
optimizer = torch.optim.Adam(model.parameters(), lr=0.01)


def train(epoch):
    # 训练模型
    for batch_idx, (input, target) in enumerate(train_loader):
        input, target = input.to(device), target.to(device)
        prediction = model(input)
        loss = loss_func(prediction, target)

        loss_list.append(loss.item())
        plt.plot([i for i in range(len(loss_list))], loss_list)
        plt.show()
        plt.pause(0.1)

        print(epoch, batch_idx, loss.item())

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()


def eval(epoch):
    # 测试模型 评价指标 绘图
    with torch.no_grad():  # 强制之后的内容不进行计算图构建
        print('+' * 20)
        for batch_idx, (input, target) in enumerate(validation_loader):
            input, target = input.to(device), target.to(device)
            prediction = model(input)
            loss = loss_func(prediction, target)
            # acc = accuracy_score(prediction, target)  # sklearn的库函数accuracy_score()只能使用cpu计算

            # torch.where(),: y_label=parameter1 if pre>=0.5 else parameter2
            # 下面两行代码可以使用cuda计算
            y_label = torch.where(prediction >= 0.5, torch.Tensor([1.0]).to(device), torch.Tensor([0.0]).to(device))
            acc = torch.eq(y_label, target).sum().item() / target.size(0)
            print(epoch, batch_idx, loss.item(), acc)
        print('+' * 20)


def get_output(Id_Survived, pre_y):
    survived = pre_y.squeeze().cpu().numpy()
    survived = np.where(survived >= 0.5, 1, 0).tolist()  # 将概率变为0/1，再变成列表才能加入DataFrame
    result = pd.DataFrame({'PassengerId': Id_Survived, 'Survived': survived})
    result.to_csv('titanic_predict.csv', index=False)  # 保存结果


if __name__ == '__main__':
    plt.figure(figsize=(3, 2))
    plt.ion()
    loss_list = []
    # 训练模型  ##########################################
    for epoch in range(100):
        train(epoch)
        if epoch % 10 == 9:  # 每训练100次就评估一下模型
            eval(epoch)

    # 保存整个模型
    torch.save(model, 'titanic.pkl')  # 读取用torch.load()
    #######################################################

    # 使用模型得到结果
    Id_Survived, test_set = get_test_set()  # 获取测试集
    with torch.no_grad():
        pre_y = model(test_set)
    get_output(Id_Survived, pre_y)
    plt.pause(0)
