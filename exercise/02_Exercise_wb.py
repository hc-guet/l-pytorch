import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

# 准备数据集
x = np.array([1.0, 2.0, 3.0])
y = np.array([2.0, 4.0, 6.0])


def forward(x):
    return w * x + b


def cost(pred_y, y):
    loss = (pred_y - y) ** 2
    return loss


w_list = np.arange(0.0, 4.1, 0.1)
b_list = np.arange(-2.0, 2.1, 0.1)
w, b = np.meshgrid(w_list, b_list)
mse_list = []

loss_sum = 0

# zip()函数用于将可迭代的对象作为参数，将对象中对应的元素打包成一个个元组，然后返回由这些元组组成的列表
for x_val, y_val in zip(x, y):
    prediction = forward(x_val)
    loss = cost(prediction, y_val)
    loss_sum += loss

fig = plt.figure()
ax = Axes3D(fig)
ax.plot_surface(w_list, b_list, loss_sum / len(x), cmap=plt.get_cmap('rainbow'))
plt.xlabel('w')
plt.ylabel('b')
plt.show()
