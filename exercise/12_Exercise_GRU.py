'''
Train a Embedding-RNN-Linear model to learn:
"hello" -> "ohlol"
'''

import torch

num_class = 4
input_size = 4
hidden_size = 8
embedding_size = 10
batch_size = 1
num_layers = 1
seq_len = 5  # seqLen就是单词字母个数

idx2char = ['e', 'h', 'l', 'o']  # 相当于一个字典
x_data = [[1, 0, 2, 2, 3]]  # (batch, seq_len)
y_data = [3, 1, 2, 3, 2]  # (batch * seq_len)

inputs = torch.LongTensor(x_data)
labels = torch.LongTensor(y_data)


class Model(torch.nn.Module):
    def __init__(self):
        super(Model, self).__init__()
        self.emb = torch.nn.Embedding(num_embeddings=input_size, embedding_dim=embedding_size)
        # num_embeddings(int) - 嵌入字典的大小
        # embedding_dim(int) - 每个嵌入向量的大小
        self.gru = torch.nn.GRU(input_size=embedding_size,
                                hidden_size=hidden_size,
                                num_layers=num_layers,
                                batch_first=True)
        self.fc = torch.nn.Linear(hidden_size, num_class)

    def forward(self, x):
        # x.shape = (batch, seqLen)
        hidden = torch.zeros(num_layers, batch_size, hidden_size)
        x = self.emb(x)  # (batch, seqLen, embeddingSize)
        x, _ = self.gru(x, hidden)  # (batch, seqLen, hiddenSize)
        x = self.fc(x)  # (batch, seqLen, num_class)
        return x.view(-1, num_class)  # (batch * seqLen, num_class)


model = Model()
criterion = torch.nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(model.parameters(), lr=0.05)

for epoch in range(15):
    optimizer.zero_grad()
    outputs = model(inputs)
    loss = criterion(outputs, labels)
    loss.backward()
    optimizer.step()

    _, idx = outputs.max(dim=1)
    idx = idx.data.numpy()
    print('Predicted string:', ''.join([idx2char[x] for x in idx]), end='')
    print(',Epoch [%d/15] loss=%.4f' % (epoch + 1, loss.item()))
