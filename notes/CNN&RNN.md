# CNN

**Conv2d(kernel_size=1) 主要是为了降维，减少计算量。**

**ResidualNet 残差网络，为了解决梯度消失的问题。**



# **Basic RNN**



## RNN Cell

**input:**

input.shape = (batchSize, inputSize)

hidden.shape = (batchSize, hiddenSize)

**out:**

hidden.shape = (batchSize, hiddenSize)





RNN

**input and h_0:**

input.shape = (seqLen, batchSize, inputSize)

h_0.shape = (numlayers, batchSize, hiddenSize)



**output and h_n:**

output.shape = (seqLen, batchSize, hiddenSize)

h_n.shape = (numLayers, batchSize, hiddenSize)



# Embedding

The input of Embedding Layer with shape: (seqLen, batchSize)

The output of Embedding Layer with shape: (seqLen, batchSize, hiddenSize)
