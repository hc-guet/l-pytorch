# 函数传入参数不确定

def function(x, *args, **kwargs):
    print(x)  # 传入确定的参数
    # 不确定多少个参数
    print(args)  # *封装成元组
    print(kwargs)  # **封装成字典


function(0, 1, 2, 3, 4, a=6, b=7, c=8)

# result===================
# 0
# (1, 2, 3, 4)
# {'a': 6, 'b': 7, 'c': 8}
# =========================
