import numpy as np
import matplotlib.pyplot as plt

# 准备数据集
# x = np.array([1.0, 2.0, 3.0])
# y = np.array([2.0, 4.0, 6.0])
x = [1.0, 2.0, 3.0]
y = [2.0, 4.0, 6.0]


def forward(w, x):
    return w * x


def cost(pred_y, y):
    loss = (pred_y - y) ** 2
    return loss


w_list = []
mse_list = []

for w in np.arange(0.0, 4.1, 0.1):
    print('\nw:{:.2f}'.format(w))  # 保留两位小数
    w_list.append(w)
    loss_sum = 0

    # zip()函数用于将可迭代的对象作为参数，将对象中对应的元素打包成一个个元组，然后返回由这些元组组成的列表
    for x_val, y_val in zip(x, y):
        prediction = forward(w, x_val)
        loss = cost(prediction, y_val)
        loss_sum += loss
    mse = loss_sum / len(x)
    print('mse:{:.2f}'.format(mse))
    mse_list.append(mse)

plt.figure()
plt.xlabel('w')  # x轴标签
plt.ylabel('mse')  # y轴标签
plt.plot(w_list, mse_list)
plt.show()
