# 还是随机梯度下降，只不过我们这次用torch自动求梯度

import torch

x_data = [1.0, 2.0, 3.0]
y_data = [2.0, 4.0, 6.0]

# w现在是Tensor，会自动求导，与w运算的变量也会变成Tensor
w = torch.Tensor([1.0])
w.requires_grad = True
lr = 0.01  # 学习率
EPOCH = 100


def forward(x):
    return w * x


def loss(x, y):
    pre_y = forward(x)
    return (pre_y - y) ** 2


# 现在这个函数的功能用torch的autograd代替
# def gradient(x, y):
#     grad = 2 * x * (forward(x) - y)  # 计算单个样本的梯度
#     return grad


print("w before:", w.item())
print("\nprediction before training:", 4, '{:.2f}\n'.format(forward(4).item()))
for epoch in range(EPOCH):
    for x, y in zip(x_data, y_data):
        l = loss(x, y)  # 每次计算单个样本的loss
        l.backward()  # 反向传播

        # grad_val = gradient(x, y)  # 计算一个样本的梯度
        # w -= lr * grad_val  # 根据一个样本的梯度更新w
        print('grad:', w.grad.item())
        w.data -= lr * w.grad.data  # Tensor自动计算梯度，代替上面两行代码

        w.grad.data.zero_()  # 梯度清零

    # 下面的l是一轮训练最后的(x,y)产生的loss
    print("epoch:", epoch, 'w:{:.2f}'.format(w.item()), 'loss:{:.2f}\n'.format(l.item()))

print("\nprediction after training:", 4, '{:.2f}'.format(forward(4).item()))
