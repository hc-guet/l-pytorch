import numpy as np
import torch
from torch.utils.data import Dataset, DataLoader

device = 'cuda'  # 将数据和模型放到cuda上运行，两种方式：.to(device) / .cuda()
# device = 'cpu'

# 标准数据集类
class Diabetes(Dataset):
    def __init__(self, filepath):
        xy = np.loadtxt(filepath, delimiter=',', dtype=np.float32)
        self.len = xy.shape[0]
        self.x_data = torch.from_numpy(xy[:, :-1])
        self.y_data = torch.from_numpy(xy[:, [-1]])  # xy[:,[-1]]最后取出的是矩阵

    def __getitem__(self, index):
        return self.x_data[index], self.y_data[index]

    def __len__(self):
        return self.len


diabetes = Diabetes('../dataset/diabetes.csv.gz')

# 数据加载对象
loader = DataLoader(
    dataset=diabetes,
    batch_size=32,
    shuffle=True,
    num_workers=2  # 多线程并行读取数据构成mini-batch,CPU核数相关
)


class Model(torch.nn.Module):
    def __init__(self):
        super(Model, self).__init__()
        self.linear1 = torch.nn.Linear(8, 6)
        self.linear2 = torch.nn.Linear(6, 4)
        self.linear3 = torch.nn.Linear(4, 1)
        self.sigmiod = torch.nn.Sigmoid()

    def forward(self, x):
        x = self.sigmiod(self.linear1(x))
        x = self.sigmiod(self.linear2(x))
        x = self.sigmiod(self.linear3(x))
        return x


model = Model().to(device)
criterion = torch.nn.BCELoss()
opitimizer = torch.optim.Adam(model.parameters(), lr=0.1)  # Adam优化器效果比较好

if __name__ == '__main__':  # windows用户如果要使用多核多线程必须把训练放在if __name__ == '__main__':下才不会报错
    for epoch in range(10000):
        for i, (x_data, y_data) in enumerate(loader):
            x_data, y_data = x_data.to(device), y_data.to(device)
            pre_y = model(x_data)  # 一次放入了所有数据
            loss = criterion(pre_y, y_data)
            print(epoch, i, loss.item())
            opitimizer.zero_grad()
            loss.backward()
            opitimizer.step()
        if epoch % 1000 == 999:  # 每100个epoch输出训练效果
            # 计算准确率acc
            # torch.where(),: y_label=parameter1 if pre>=0.5 else parameter2
            y_label = torch.where(pre_y >= 0.5, torch.Tensor([1.0]).to(device), torch.Tensor([0.0]).to(device))
            acc = torch.eq(y_label, y_data).sum().item() / y_data.size(0)
            print(epoch, loss.item(), acc)  # use cuda computing
