'''
Train a RNN model to learn:
"hello" -> "ohlol"
'''

import torch

input_size = 4
hidden_size = 4
batch_size = 1
num_layers = 1
# seqLen就是单词字母个数

idx2char = ['e', 'h', 'l', 'o']  # 相当于一个字典
x_data = [1, 0, 2, 2, 3]  # "hello"从idx2char里面取index
y_data = [3, 1, 2, 3, 2]  # "ohlol"从idx2char里面取index

# 定义一个独热码查询表
one_hot_lookup = [
    [1, 0, 0, 0],
    [0, 1, 0, 0],
    [0, 0, 1, 0],
    [0, 0, 0, 1]
]

# 将 x_data 转换为 one_hot 向量
x_one_hot = [one_hot_lookup[x] for x in x_data]

# (seqLen, batchSize, inputSize) seqLen就是字母个数，一个个输入RNN
inputs = torch.Tensor(x_one_hot).view(-1, batch_size, input_size)
# 每个inputSize对应一个index
labels = torch.LongTensor(y_data)  # (seqLen*batchSize)


class Model(torch.nn.Module):
    def __init__(self, input_size, hidden_size, batch_size, num_layers):
        super(Model, self).__init__()
        self.num_layers = num_layers
        self.batch_size = batch_size
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.rnn = torch.nn.RNN(input_size=self.input_size,
                                hidden_size=self.hidden_size,
                                num_layers=self.num_layers)

    def forward(self, input):
        hidden = torch.zeros(self.num_layers,
                             self.batch_size,
                             self.hidden_size)
        out, _ = self.rnn(input, hidden)
        return out.view(-1, self.hidden_size)  # (seqLen*batchSize, hiddenSize)


model = Model(input_size, hidden_size, batch_size, num_layers)
criterion = torch.nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(model.parameters(), lr=0.05)

for epoch in range(15):
    optimizer.zero_grad()
    outputs = model(inputs)
    loss = criterion(outputs, labels)
    loss.backward()
    optimizer.step()
    _, idx = outputs.max(dim=1)
    idx = idx.data.numpy()
    print('Predicted string:', ''.join([idx2char[x] for x in idx]), end='')
    print(',Epoch [%d/15] loss=%.4f' % (epoch + 1, loss.item()))
