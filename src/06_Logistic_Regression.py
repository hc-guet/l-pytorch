from abc import ABC

import torch
import numpy as np
import matplotlib.pyplot as plt

x_data = torch.Tensor([[1.0], [2.0], [3.0]])
y_data = torch.Tensor([[0], [0], [1]])


class LogisticRegression(torch.nn.Module, ABC):
    def __init__(self):
        super(LogisticRegression, self).__init__()
        self.linear = torch.nn.Linear(1, 1)

    def forward(self, x):
        pred_y = torch.sigmoid(self.linear(x))
        return pred_y


model = LogisticRegression()
criterion = torch.nn.BCELoss()
optimizer = torch.optim.SGD(model.parameters(), lr=0.01)

for epoch in range(100):
    pre_y = model(x_data)  # 模型预测
    loss = criterion(pre_y, y_data)  # 计算损失

    # -------------------------------------------------------------------------------------
    # loss = criterion(y_data, pre_y)   # 错误写法
    # torch.nn.BCELoss(input, target)   # 这是二分类交叉熵的参数位置
    # 报错：the derivative for 'target' is not implemented
    # 因为 input = model(x), BCELoss只对input求导，如果把model(x)写到target位置，就会提示求导没有实现
    # -------------------------------------------------------------------------------------

    print(epoch, '{:.2f}'.format(loss.item()))  # 当loss=0.5时

    optimizer.zero_grad()  # 梯度先清零
    loss.backward()  # 反向传播
    optimizer.step()  # 更新参数

x = np.linspace(0, 10, 200)
x_t = torch.Tensor(x).view((200, 1))  # 变为200行1列
y = model(x_t)
y = y.data.numpy()

plt.plot(x, y)
plt.plot([0, 10], [0.5, 0.5], c='r')
plt.xlabel('Hours')
plt.ylabel('Probability of Pass')
plt.grid()
plt.show()
