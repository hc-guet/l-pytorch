import numpy as np
import torch
from sklearn.metrics import accuracy_score

device = 'cuda'  # 将数据和模型放到cuda上运行，两种方式：.to(device) / .cuda()
# device = 'cpu'

xy = np.loadtxt('../dataset/diabetes.csv.gz', delimiter=',', dtype=np.float32)
x_data = torch.from_numpy(xy[:, :-1]).to(device)
y_data = torch.from_numpy(xy[:, [-1]]).to(device)  # xy[:,[-1]]最后取出的是矩阵


class Model(torch.nn.Module):
    def __init__(self):
        super(Model, self).__init__()
        self.linear1 = torch.nn.Linear(8, 6)
        self.linear2 = torch.nn.Linear(6, 4)
        self.linear3 = torch.nn.Linear(4, 1)
        self.sigmiod = torch.nn.Sigmoid()

    def forward(self, x):
        x = self.sigmiod(self.linear1(x))
        x = self.sigmiod(self.linear2(x))
        x = self.sigmiod(self.linear3(x))
        return x


model = Model().to(device)
criterion = torch.nn.BCELoss()
opitimizer = torch.optim.Adam(model.parameters(), lr=0.1)  # Adam优化器效果比较好

for epoch in range(10000):
    pre_y = model(x_data)  # 一次放入了所有数据
    loss = criterion(pre_y, y_data)
    # print(epoch, loss.item())
    opitimizer.zero_grad()
    loss.backward()
    opitimizer.step()
    if epoch % 100 == 99:  # 每100个epoch输出训练效果
        # 计算准确率acc
        # torch.where(),: y_label=parameter1 if pre>=0.5 else parameter2
        y_label = torch.where(pre_y >= 0.5, torch.Tensor([1.0]).to(device), torch.Tensor([0.0]).to(device))
        acc1 = torch.eq(y_label, y_data).sum().item() / y_data.size(0)

        # acc2 = accuracy_score(y_label, y_data)  # 调用sklearn库函数来计算acc, 这里只能接受CPU数据

        # print(epoch, loss.item(), acc1, acc2)
        print(epoch, loss.item(), acc1)  # use cuda computing
