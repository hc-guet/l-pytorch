import torch
from torchvision import transforms, datasets
from torch.utils.data import DataLoader

batch_size = 64

transform = transforms.Compose([
    transforms.ToTensor(),  # 转化为Tensor
    transforms.Normalize((0.1307,), (0.3081,))  # 归一化，两个参数分别为均值和标准差
])

# 训练集和测试集数据对象
train_data = datasets.MNIST(root='../dataset/mnist',
                            train=True,
                            transform=transform,
                            download=True)

test_data = datasets.MNIST(root='../dataset/mnist',
                           train=False,
                           transform=transform,
                           download=True)

# 训练集测试集数据加载对象
train_loader = DataLoader(
    dataset=train_data,
    shuffle=True,
    batch_size=batch_size,

)

test_loader = DataLoader(
    dataset=test_data,
    batch_size=batch_size,
    shuffle=False,  # 测试集不打乱有利于观察结果
)


class MyModel(torch.nn.Module):
    def __init__(self):
        super(MyModel, self).__init__()
        self.l1 = torch.nn.Linear(784, 512)
        self.l2 = torch.nn.Linear(512, 256)
        self.l3 = torch.nn.Linear(256, 128)
        self.l4 = torch.nn.Linear(128, 64)
        self.l5 = torch.nn.Linear(64, 10)

    def forward(self, x):
        x = x.view(-1, 784)
        x = torch.relu(self.l1(x))
        x = torch.relu(self.l2(x))
        x = torch.relu(self.l3(x))
        x = torch.relu(self.l4(x))
        return self.l5(x)


model = MyModel().cuda()
loss_func = torch.nn.CrossEntropyLoss()
optimizer = torch.optim.SGD(model.parameters(), lr=0.01, momentum=0.5)  # SGD加上动量训练效果更好


def train(epoch):
    # 训练模型
    running_loss = 0.0
    for batch_idx, (input, target) in enumerate(train_loader):
        images, labels = input.cuda(), target.cuda()
        prediction = model(images)
        loss = loss_func(prediction, labels)

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        running_loss += loss.item()
        if batch_idx % 300 == 299:
            print('[%d, %5d] loss: %.3f' % (epoch + 1, batch_idx + 1, running_loss / 300))
            running_loss = 0.0


def eval(epoch):
    # 测试模型 评价指标
    correct = 0
    total = 0
    with torch.no_grad():  # 强制之后的内容不进行计算图构建。
        for batch_idx, (input, target) in enumerate(test_loader):
            images, labels = input.cuda(), target.cuda()
            output = model(images)
            _, prediction = torch.max(output.data, dim=1)  # torch.max()返回最大值和最大值对应的下标
            total += labels.size(0)
            correct += (prediction == labels).sum().item()
    print('epoch: %d, acc on test dataset: %d %%' % (epoch, 100 * correct / total))


if __name__ == '__main__':
    for epoch in range(100):
        train(epoch)
        if epoch % 10 == 9:  # 每训练10次就评估一下模型
            eval(epoch)
