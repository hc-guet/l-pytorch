import torch

x_data = torch.Tensor([[1.0], [2.0], [3.0]])
y_data = torch.Tensor([[2.0], [4.0], [6.0]])


class LinearModel(torch.nn.Module):
    def __init__(self):
        super(LinearModel, self).__init__()
        self.linear = torch.nn.Linear(1, 1)

    def forward(self, x):
        pred_y = self.linear(x)
        return pred_y


model = LinearModel()
criterion = torch.nn.MSELoss()
optimizer = torch.optim.SGD(model.parameters(), lr=0.01)

x_test = torch.Tensor([[4.0]])
y_prediction = model(x_test)
print('prediction before training:{:.2f}'.format(y_prediction.item()))

for epoch in range(1000):
    pre_y = model(x_data)  # 模型预测
    loss = criterion(pre_y, y_data)  # 计算损失
    print(epoch, '{:.2f}'.format(loss.item()))

    optimizer.zero_grad()  # 梯度先清零
    loss.backward()  # 反向传播
    optimizer.step()  # 更新参数

print("w={:.2f}".format(model.linear.weight.item()))
print("b={:.2f}".format(model.linear.bias.item()))

y_prediction = model(x_test)
print('prediction after training:{:.2f}'.format(y_prediction.item()))
