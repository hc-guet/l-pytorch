# 根据第二课线性模型所画的图
# 我们要找到mse最小的点对应的w值，也就是梯度为0的w值
import matplotlib.pyplot as plt

x_data = [1.0, 2.0, 3.0]
y_data = [2.0, 4.0, 6.0]

w = 1.0  # 权重初始值
lr = 0.01  # 学习率
EPOCH = 100


def forward(x):
    return w * x


def cost(xs, ys):
    loss_sum = 0
    for x, y in zip(xs, ys):
        pred_y = forward(x)
        loss_sum += (pred_y - y) ** 2
    mse = loss_sum / len(xs)  # 计算均方差
    return mse


def gradient(xs, ys):
    grad_sum = 0
    for x, y in zip(xs, ys):
        grad_sum += 2 * x * (forward(x) - y)
    grad = grad_sum / len(xs)  # 计算梯度
    return grad


cost_list = []

print("\nw before:", w)
print("\nprediction before training:", 4, forward(4))
for epoch in range(EPOCH):
    cost_val = cost(x_data, y_data)
    cost_list.append(cost_val)
    grad_val = gradient(x_data, y_data)
    w -= lr * grad_val  # 更新w
    print("\nepoch:", epoch, 'w:{:.2f}'.format(w), 'cost:{:.2f}'.format(cost_val))

print("\nprediction after training:", 4, '{:.2f}'.format(forward(4)))

# [epoch for epoch in range(EPOCH)]
# 这句代码可以快速创建列表 [0, 1, 2, ..., EPOCH-1]
plt.plot([epoch for epoch in range(EPOCH)], cost_list)
plt.xlabel('Epoch')
plt.ylabel('Cost')
plt.grid(True)
plt.show()
